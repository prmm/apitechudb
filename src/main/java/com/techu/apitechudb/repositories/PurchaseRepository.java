package com.techu.apitechudb.repositories;



import com.techu.apitechudb.models.purchaseModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseRepository extends MongoRepository<purchaseModel, String>{

}
