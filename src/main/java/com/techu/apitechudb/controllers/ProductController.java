package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");


        return new ResponseEntity<>(
        this.productService.findAll(),
                HttpStatus.OK);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductsById(@PathVariable String id){
        System.out.println("getProductsByid");
        System.out.println(" ID " + id);

        Optional<ProductModel> result = this.productService.findID(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProducts(@RequestBody ProductModel product){
        System.out.println("addProducts");
        System.out.println(" ID ddProducts" + product.getId());
        System.out.println(" DESC addProducts" + product.getDesc());
        System.out.println("price addProducts" + product.getPrice());


        return new ResponseEntity<>(
                this.productService.add(product),
                HttpStatus.CREATED);
    }


    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateproduct(@PathVariable String id, @RequestBody ProductModel product){
        System.out.println("updateproduct");
        System.out.println(" ID " + id);

        Optional<ProductModel> result = this.productService.findID(id);

        if(result.isPresent()){
            this.productService.update(product);
        }

        return new ResponseEntity<>(
                result.isPresent() ? product: "Producto no Encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<Object> DelProductsById(@PathVariable String id){
        System.out.println("DelProductsById");
        System.out.println(" ID " + id);

        Optional<ProductModel> result = this.productService.findID(id);

        System.out.println("DelProductsById");

        if (result.isPresent()) {
            System.out.println("Producto encontrado");
            this.productService.delete(id);
        } else {
            System.out.println("Producto no encontrado");
        }

        result = this.productService.findID(id);

        return new ResponseEntity<>(
                result.isPresent() ? "Producto no encontrado" : "Producto eliminado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }



}
