package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.purchaseModel;
import com.techu.apitechudb.services.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE})
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchases")
    @Query
    public ResponseEntity<List<purchaseModel>> getpurchases(@RequestParam( name = "$orderby" , required = false) String orderBy){
        System.out.println("getUsers");

        System.out.println(orderBy);
        if(orderBy != null){
            System.out.println("getUsers Sort");
            return new ResponseEntity<>(
                    this.purchaseService.findAllOrder(orderBy),
                    HttpStatus.OK);
        }else{
            return new ResponseEntity<>(
                    this.purchaseService.findAll(),
                    HttpStatus.OK);
        }

    }

    @GetMapping("/purchases/{id}{}")
    public ResponseEntity<Object> getpurchaseById(@PathVariable String id){
        System.out.println("getpurchaseByid");
        System.out.println(" ID " + id);

        Optional<purchaseModel> result = this.purchaseService.findID(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "compra no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/purchases")
    public ResponseEntity<purchaseModel> addPurchases(@RequestBody purchaseModel purchase){
        System.out.println("addPurchases");
        System.out.println(" ID de purchase" + purchase.getId());
        System.out.println(" userID addUsers" + purchase.getUserID());
        System.out.println(" total addUsers" + purchase.getTotal());


        return new ResponseEntity<>(
                this.purchaseService.add(purchase),
                HttpStatus.CREATED);
    }


    @PutMapping("/purchases/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable String id, @RequestBody purchaseModel purchase){
        System.out.println("updateUser");
        System.out.println(" ID " + id);
        boolean actualizado = false;

        Optional<purchaseModel> result = this.purchaseService.findID(id);

        if(result.isPresent() && id.equals(purchase.getId())){
            this.purchaseService.update(purchase);
            actualizado = true;
        }

        return new ResponseEntity<>(
                actualizado ? purchase: "compra no Encontrado",
                actualizado ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/purchases/{id}")
    public ResponseEntity<Object> DelpurchaseById(@PathVariable String id){
        System.out.println("DelpurchasesById");
        System.out.println(" ID " + id);
        boolean eliminado = false;

        Optional<purchaseModel> result = this.purchaseService.findID(id);

        if (result.isPresent()) {
            System.out.println("compra encontrado");
            this.purchaseService.delete(id);
            eliminado = true;
        } else {
            System.out.println("compra no encontrado");
        }


        return new ResponseEntity<>(
                eliminado ? "compra eliminado": "compra no encontrado",
                eliminado ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }



}
