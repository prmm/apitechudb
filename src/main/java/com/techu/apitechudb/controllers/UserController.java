package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE})
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    @Query
    public ResponseEntity<List<UserModel>> getUsers( @RequestParam( name = "$orderby" , required = false) String orderBy){
        System.out.println("getUsers");

        System.out.println(orderBy);
        if(orderBy != null){
            System.out.println("getUsers Sort");
            return new ResponseEntity<>(
                    this.userService.findAllOrder(orderBy),
                    HttpStatus.OK);
        }else{
            return new ResponseEntity<>(
                    this.userService.findAll(),
                    HttpStatus.OK);
        }

    }

    @GetMapping("/users/{id}{}")
    public ResponseEntity<Object> getusersById(@PathVariable String id){
        System.out.println("getusersByid");
        System.out.println(" ID " + id);

        Optional<UserModel> result = this.userService.findID(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUsers(@RequestBody UserModel user){
        System.out.println("addUsers");
        System.out.println(" ID de users" + user.getId());
        System.out.println(" DESC addUsers" + user.getName());
        System.out.println("price addUsers" + user.getAge());


        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED);
    }


    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable String id, @RequestBody UserModel user){
        System.out.println("updateUser");
        System.out.println(" ID " + id);
        boolean actualizado = false;

        Optional<UserModel> result = this.userService.findID(id);

        if(result.isPresent() && id.equals(user.getId())){
            this.userService.update(user);
            actualizado = true;
        }

        return new ResponseEntity<>(
                actualizado ? user: "usuario no Encontrado",
                actualizado ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Object> DelusersById(@PathVariable String id){
        System.out.println("DelUsersById");
        System.out.println(" ID " + id);
        boolean eliminado = false;

        Optional<UserModel> result = this.userService.findID(id);

        if (result.isPresent()) {
            System.out.println("usuario encontrado");
            this.userService.delete(id);
            eliminado = true;
        } else {
            System.out.println("usuario no encontrado");
        }


        return new ResponseEntity<>(
                eliminado ? "usuario eliminado": "usuario no encontrado",
                eliminado ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }



}
