package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UserService {

        @Autowired
        UserRepository userRepository;

        public List<UserModel> findAll(){
            System.out.println("findAll en UserService");

            return userRepository.findAll();
        }

        public List<UserModel> findAllOrder(String order){
            System.out.println("findAll ORDER : "+ order+" en UserService");

            if(order.equals("ASC")){
                System.out.println("USO EL ASC");
                return userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));
            }else {
                System.out.println("USO EL DESC");
                return userRepository.findAll(Sort.by(Sort.Direction.DESC, "age"));
            }
        }

        public UserModel add(UserModel user){
            System.out.println("add en el UserService");

            return this.userRepository.save(user);
        }

    public Optional<UserModel> findID(String id){
        System.out.println("Buscar por ID en el UserService");

        return this.userRepository.findById(id);
    }

    public UserModel update(UserModel user){
        System.out.println("update en el UserService");

        return this.userRepository.save(user);
    }

    public void delete(String id){
        System.out.println("Delete by ID UserService");

        this.userRepository.deleteById(id);
    }

}


//BasicCalculator sut =