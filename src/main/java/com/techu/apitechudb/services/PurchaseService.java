package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.models.purchaseModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;


@Service
public class PurchaseService {

        @Autowired
        PurchaseRepository purchaseRepository;

        @Autowired
        UserService userService;


        @Autowired
        ProductService productService;

        public List<purchaseModel> findAll(){
            System.out.println("findAll en PurchaseService)");

            return purchaseRepository.findAll();
        }

        public purchaseModel add(purchaseModel purchase){
            System.out.println("add en el PurchaseService");

            if(this.userService.findID(purchase.getUserID()).isPresent() == true){
                System.out.println("El usuario de la compra no se ha encontrado");

                return null;
            }

            if(this.purchaseRepository.findById(purchase.getId()).isPresent() == true){
                System.out.println("El id de la compra ya existe");
                return null;
            }

            //otras comprobaciones antes de grabar
            return this.purchaseRepository.save(purchase);
        }

    public Optional<purchaseModel> findID(String id){
        System.out.println("Buscar por ID en el PurchaseService");

        return this.purchaseRepository.findById(id);
    }

    public purchaseModel update(purchaseModel purchase){
        System.out.println("update en el PurchaseService");

        return this.purchaseRepository.save(purchase);
    }

    public void delete(String id){
        System.out.println("Delete by ID");

        this.purchaseRepository.deleteById(id);
    }

    public List<purchaseModel> findAllOrder(String order){
        System.out.println("findAll ORDER : "+ order+" en PurchaseService");

        if(order.equals("ASC")){
            System.out.println("USO EL ASC");
            return purchaseRepository.findAll(Sort.by(Sort.Direction.ASC, "total"));
        }else {
            System.out.println("USO EL DESC");
            return purchaseRepository.findAll(Sort.by(Sort.Direction.DESC, "total"));
        }
    }

    public PurchaseServiceResponse addPurchase(purchaseModel purchase) {
        System.out.println("addPurchase");

        PurchaseServiceResponse result = new PurchaseServiceResponse();

        result.setPurchase(purchase);
        if (this.userService.findID(purchase.getUserID()).isPresent() == true) {
            System.out.println("El usuario de la compra no se ha encontrado");

            result.setMsg("El usuario de la compra no se ha encontrado");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }
        if (this.purchaseRepository.findById(purchase.getId()).isPresent() == true) {
            System.out.println("Ya hay una compra con esa id");

            result.setMsg("Ya hay una compra con esa id");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }
        float amount = 0;
        for (Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()) {
            if (this.productService.findID(purchaseItem.getKey()).isPresent()) {
                System.out.println("El producto con la id " + purchaseItem.getKey()
                        + " no se encuentra en el sistema");

                result.setMsg("El producto con la id " + purchaseItem.getKey()
                        + " no se encuentra en el sistema");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
                return result;
            } else {
                System.out.println("Añadiendo valor de " + purchaseItem.getValue()
                        + " unidades del producto al total");

                amount +=
                        (this.productService.findID(purchaseItem.getKey()).get().getPrice()
                                * purchaseItem.getValue());
            }
        }
        purchase.setTotal(amount);
        this.purchaseRepository.save(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.CREATED);

        return result;
    }

}
