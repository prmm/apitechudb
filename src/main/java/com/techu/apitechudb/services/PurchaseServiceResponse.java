package com.techu.apitechudb.services;

import com.techu.apitechudb.models.purchaseModel;


import org.springframework.http.HttpStatus;


import java.util.List;
import java.util.Optional;

public class PurchaseServiceResponse {

        private String msg;
        private purchaseModel purchase;
        private HttpStatus responseHttpStatusCode;

    public PurchaseServiceResponse() {
    }

    public PurchaseServiceResponse(String msg, purchaseModel purchase, HttpStatus responseHttpStatusCode) {
        this.msg = msg;
        this.purchase = purchase;
        this.responseHttpStatusCode = responseHttpStatusCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public purchaseModel getPurchase() {
        return purchase;
    }

    public void setPurchase(purchaseModel purchase) {
        this.purchase = purchase;
    }

    public HttpStatus getResponseHttpStatusCode() {
        return responseHttpStatusCode;
    }

    public void setResponseHttpStatusCode(HttpStatus responseHttpStatusCode) {
        this.responseHttpStatusCode = responseHttpStatusCode;
    }
}
