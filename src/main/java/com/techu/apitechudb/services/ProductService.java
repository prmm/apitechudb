package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ProductService {

        @Autowired
        ProductRepository productRepository;

        public List<ProductModel> findAll(){
            System.out.println("findAll en ProductService)");

            return productRepository.findAll();
        }

        public ProductModel add(ProductModel product){
            System.out.println("add en el Productservice");

            return this.productRepository.save(product);
        }

    public Optional<ProductModel> findID(String id){
        System.out.println("Buscar por ID en el Productservice");

        return this.productRepository.findById(id);
    }

    public ProductModel update(ProductModel product){
        System.out.println("update en el Productservice");

        return this.productRepository.save(product);
    }

    public void delete(String id){
        System.out.println("Delete by ID");

        this.productRepository.deleteById(id);
    }

}
